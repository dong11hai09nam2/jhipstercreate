package com.mycompany.myapp.service;

import com.mycompany.myapp.JhipsterApp;
import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.enumeration.Gender;
import com.mycompany.myapp.repository.CandidateCriteriaRepository;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.service.dto.CandidateDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = JhipsterApp.class)
public class CandidateSpringBootTest {
    @Autowired
    private CandidateService candidateService;

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private CandidateCriteriaRepository candidateCriteriaRepository;



    @BeforeEach
    private void init() {
        List<Candidate> candidateList = new ArrayList<>();
        Candidate candidate1 = new Candidate();
        candidate1.setFirstName("John1");
        candidate1.setLastName("Doe");
        candidate1.setBirthYear(2000);
        candidate1.setGender(Gender.Male);
        candidate1.setPersonalNumber("123456789123");
        candidate1.setRegion("Sweden");
        candidate1.setEmployer("FPT");
        candidate1.setFirstEmployment("dev");
        candidate1.setStreetAddress("Ha Noi");
        candidate1.setZipCode(1600);
        candidate1.setPhoneNumber("0123456789");
        candidate1.setEmailPrivate("private@gmail.com");
        candidate1.setEmailWork("work@techlead.com");
        candidate1.setCity("Ha noi");
        candidateList.add(candidate1);

        Candidate candidate2 = new Candidate();
        candidate2.setFirstName("John2");
        candidate2.setLastName("Doe");
        candidate2.setBirthYear(1998);
        candidate2.setGender(Gender.Female);
        candidate2.setPersonalNumber("123987654321");
        candidate2.setRegion("Sweden");
        candidate2.setEmployer("Tech lead");
        candidate2.setFirstEmployment("dev");
        candidate2.setStreetAddress("Ha Noi");
        candidate2.setZipCode(1600);
        candidate2.setPhoneNumber("9876543210");
        candidate2.setEmailPrivate("private@gmail.com");
        candidate2.setEmailWork("work@techlead.com");
        candidate2.setCity("Ha noi");
        candidateList.add(candidate2);

        Candidate candidate3 = new Candidate();
        candidate3.setFirstName("John3");
        candidate3.setLastName("Doe");
        candidate3.setBirthYear(1998);
        candidate3.setGender(Gender.Female);
        candidate3.setPersonalNumber("546789123159");
        candidate3.setRegion("Sweden");
        candidate3.setEmployer("FPT");
        candidate3.setFirstEmployment("dev");
        candidate3.setStreetAddress("Ha Noi");
        candidate3.setZipCode(1600);
        candidate3.setPhoneNumber("9876543210");
        candidate3.setEmailPrivate("private@gmail.com");
        candidate3.setEmailWork("work@fpt.com");
        candidate3.setCity("Ha noi");
        candidateList.add(candidate3);

        Candidate candidate4 = new Candidate();
        candidate4.setFirstName("John4");
        candidate4.setLastName("Doe");
        candidate4.setBirthYear(1998);
        candidate4.setGender(Gender.Male);
        candidate4.setPersonalNumber("741852963357");
        candidate4.setRegion("Sweden");
        candidate4.setEmployer("Viettel");
        candidate4.setFirstEmployment("dev");
        candidate4.setZipCode(1600);
        candidate4.setStreetAddress("Ha Noi");
        candidate4.setPhoneNumber("9876543210");
        candidate4.setEmailPrivate("private@gmail.com");
        candidate4.setEmailWork("work@fpt.com");
        candidate4.setCity("Ha noi");
        candidateList.add(candidate4);

        Candidate candidate5 = new Candidate();
        candidate5.setFirstName("John5");
        candidate5.setLastName("Doe");
        candidate5.setBirthYear(1998);
        candidate5.setGender(Gender.Male);
        candidate5.setPersonalNumber("741852963123");
        candidate5.setRegion("Sweden");
        candidate5.setDeleted(true);
        candidate5.setEmployer("Viettel");
        candidate5.setFirstEmployment("dev");
        candidate5.setStreetAddress("Ha Noi");
        candidate5.setZipCode(1600);
        candidate5.setPhoneNumber("9876543210");
        candidate5.setEmailPrivate("private@gmail.com");
        candidate5.setEmailWork("work@fpt.com");
        candidate5.setCity("Ha noi");
        candidateList.add(candidate5);

        candidateRepository.saveAll(candidateList);
    }

    @AfterEach
    public void destroy(){
        candidateRepository.deleteAll();
    }

    @Test
    public void testFindCandidates(){

        Page<Candidate> page = candidateRepository.findCandidateByDeletedIsFalse(PageRequest.of(0,20));

        assertThat(page.getContent().size()).isEqualTo(4);
        assertThat(page.getContent().get(0).getFirstName()).isEqualTo("John1");
        assertThat(page.getContent().get(0).getLastName()).isEqualTo("Doe");
        assertThat(page.getContent().get(0).getBirthYear()).isEqualTo(2000);
        assertThat(page.getContent().get(0).getGender()).isEqualTo(Gender.Male);
        assertThat(page.getContent().get(0).getPersonalNumber()).isEqualTo("123456789123");
        assertThat(page.getContent().get(0).getEmployer()).isEqualTo("FPT");
        assertThat(page.getContent().get(0).getFirstEmployment()).isEqualTo("dev");
        assertThat(page.getContent().get(0).getStreetAddress()).isEqualTo("Ha Noi");
        assertThat(page.getContent().get(0).getEmailPrivate()).isEqualTo("private@gmail.com");
        assertThat(page.getContent().get(0).getCity()).isEqualTo("Ha noi");

    }

    @Test
    public void testFindCandidatesByCriteria(){
        String keyword = "John1";

        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setFirstName("Jo");
        candidateDTO.setLastName("D");
        candidateDTO.setBirthYear(2000);
        candidateDTO.setPersonalNumber("123");
        candidateDTO.setGender(Gender.Male);
        Page<Candidate> page = candidateCriteriaRepository.findCandidateCriteria(PageRequest.of(0,20), candidateDTO, keyword);

        assertThat(page.getContent().size()).isEqualTo(1);
        assertThat(page.getContent().get(0).getFirstName()).isEqualTo("John1");
        assertThat(page.getContent().get(0).getLastName()).isEqualTo("Doe");
        assertThat(page.getContent().get(0).getBirthYear()).isEqualTo(2000);
        assertThat(page.getContent().get(0).getGender()).isEqualTo(Gender.Male);
        assertThat(page.getContent().get(0).getPersonalNumber()).isEqualTo("123456789123");
        assertThat(page.getContent().get(0).getEmployer()).isEqualTo("FPT");
        assertThat(page.getContent().get(0).getFirstEmployment()).isEqualTo("dev");
        assertThat(page.getContent().get(0).getStreetAddress()).isEqualTo("Ha Noi");
        assertThat(page.getContent().get(0).getEmailPrivate()).isEqualTo("private@gmail.com");
        assertThat(page.getContent().get(0).getCity()).isEqualTo("Ha noi");

    }
}
