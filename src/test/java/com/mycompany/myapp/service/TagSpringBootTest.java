package com.mycompany.myapp.service;

import com.mycompany.myapp.JhipsterApp;
import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.repository.TagRepository;
import com.mycompany.myapp.repository.TagTrackingRepository;
import com.mycompany.myapp.service.dto.TagDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(classes = JhipsterApp.class)
public class TagSpringBootTest {
    @Autowired
    UserACL userACL;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    TagTrackingService tagTrackingService;

    @Autowired
    TagTrackingRepository tagTrackingRepository;

    @BeforeEach
    public void init() {
        List<Tag> tagList = new ArrayList<>();

        Tag tag1 = new Tag();
        tag1.setValue("tag1");
        tag1.setDeleted(true);
        tag1.setActive(true);
        tag1.setCountSearch(5L);
        tagList.add(tag1);

        Tag tag2 = new Tag();
        tag2.setValue("tag2");
        tag2.setDeleted(false);
        tag2.setActive(true);
        tag2.setCountSearch(5L);
        tagList.add(tag2);

        Tag tag3 = new Tag();
        tag3.setValue("tag3");
        tag3.setDeleted(false);
        tag3.setActive(true);
        tag3.setCountSearch(5L);
        tagList.add(tag3);

        Tag tag4 = new Tag();
        tag4.setValue("tag4");
        tag4.setDeleted(false);
        tag4.setActive(true);
        tag4.setCountSearch(5L);
        tagList.add(tag4);

        Tag tag5 = new Tag();
        tag5.setValue("tag5");
        tag5.setDeleted(true);
        tag5.setActive(true);
        tag5.setCountSearch(5L);
        tagList.add(tag5);

        Tag tag6 = new Tag();
        tag6.setValue("tag6");
        tag6.setDeleted(false);
        tag6.setActive(false);
        tag6.setCountSearch(4L);
        tagList.add(tag6);

        tagRepository.saveAll(tagList);
    }

    @AfterEach
    public void destroy() {
        tagRepository.deleteAll();
    }

    @Test
    public void testGetListTag() {
        Page<TagDTO> page = tagRepository.findTagsByDeletedIsFalseAndActiveIsTrue(PageRequest.of(0,20));

        assertThat(page.getContent().size()).isEqualTo(3);
        assertThat(page.getContent().get(0).getValue()).isEqualTo("tag2");
    }
}
