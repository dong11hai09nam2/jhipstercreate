package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.domain.TagTracking;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.TagTrackingRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class TagTrackingMockTest {
    @InjectMocks
    TagTrackingService tagTrackingService;

    @Mock
    UserACL userACL;

    @Mock
    TagTrackingRepository tagTrackingRepository;

    @Test
    public void testSave() {
        // Given
        User user = new User();
        user.setId(1L);

        Tag tag = new Tag();
        tag.setValue("value");

        given(tagTrackingRepository.findTagTrackingByUserAndTag(user, tag)).willReturn(Optional.empty());

        //When
        TagTracking result = tagTrackingService.save(user, tag);

        // Then
        assertThat(result.getTag().getValue()).isEqualTo("value");
    }
}
