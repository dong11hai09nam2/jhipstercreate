package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import com.mycompany.myapp.service.error.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class RequirementProfileMockTest {

    @InjectMocks
    RequirementProfileService requirementProfileService;

    @Mock
    UserACL userACL;

    @Mock
    UserRepository userRepository;

    @Mock
    RequirementProfileRepository requirementProfileRepository;

    /** ------------------------------------------------------- */
    /** --------------- saveRequirementProfile ---------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testSaveRequirementProfileWithNonUser() {
        // Given
        given(userACL.isUser()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> requirementProfileService.saveRequirementProfile(new RequirementProfileDTO()));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }

    @Test
    public void testSaveRequirementProfileSuccess() {
        // Given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        RequirementProfileDTO requirementProfileDTO = new RequirementProfileDTO();
        requirementProfileDTO.setUserId(1L);
        requirementProfileDTO.setName("name");

        // When
        RequirementProfileDTO result = requirementProfileService.saveRequirementProfile(requirementProfileDTO);

        // Then
        assertThat(result.getName()).isEqualTo("name");
    }

    /** ------------------------------------------------------- */
    /** -------------- updateRequirementProfile --------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testUpdateRequirementProfileWithNonUser() {
        // Given
        given(userACL.isUser()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> requirementProfileService.updateRequirementProfile(new RequirementProfileDTO()));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }

    @Test
    public void testUpdateRequirementProfileWithCaseNotFound() {
        // Given
        given(userACL.isUser()).willReturn(true);

        RequirementProfileDTO requirementProfileDTO = new RequirementProfileDTO();
        requirementProfileDTO.setId(1L);

        given(requirementProfileRepository.findById(1L)).willReturn(Optional.empty());

        // When
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class,
            () -> requirementProfileService.updateRequirementProfile(requirementProfileDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    @Test
    public void testUpdateRequirementProfileSuccess() {
        // Given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        RequirementProfile requirementProfile = new RequirementProfile();
        requirementProfile.setUser(user);
        requirementProfile.setName("name");

        RequirementProfileDTO requirementProfileDTO = new RequirementProfileDTO();
        requirementProfileDTO.setId(2L);
        requirementProfileDTO.setName("Name");

        given(requirementProfileRepository.findById(2L)).willReturn(Optional.of(requirementProfile));
        given(userACL.getUser()).willReturn(user);

        // When
        RequirementProfileDTO result = requirementProfileService.updateRequirementProfile(requirementProfileDTO);

        // Then
        assertThat(result.getName()).isEqualTo("Name");
    }

    /** ------------------------------------------------------- */
    /** -------------- deleteRequirementProfile --------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testDeleteRequirementProfileWithNonUser() {
        // Given
        given(userACL.isUser()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> requirementProfileService.deleteRequirementProfile(1L));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }

    @Test
    public void testDeleteRequirementProfileSuccess() {
        // Given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        RequirementProfile  requirementProfile = new RequirementProfile();
        requirementProfile.setUser(user);
        requirementProfile.setName("name");
        requirementProfile.setId(2L);

        given(userACL.getUser()).willReturn(user);
        given(requirementProfileRepository.findById(2L)).willReturn(Optional.of(requirementProfile));

        // When
        requirementProfileService.deleteRequirementProfile(2L);

        // Then
        assertThat(requirementProfile.isDeleted()).isTrue();
    }

    /** ------------------------------------------------------- */
    /** -------------- getListRequirementProfile -------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testGetListRequirementProfileWithNonUser() {
        // Given
        given(userACL.isUser()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> requirementProfileService.getListRequirementProfile(PageRequest.of(1,2)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }

    @Test
    public void testGetListRequirementProfileSuccess() {
        // Given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        List<RequirementProfileDTO> requirementProfileDTOList = new ArrayList<>();

        RequirementProfileDTO requirementProfileDTO1 = new RequirementProfileDTO();
        requirementProfileDTO1.setUserId(1L);
        requirementProfileDTOList.add(requirementProfileDTO1);

        RequirementProfileDTO requirementProfileDTO2 = new RequirementProfileDTO();
        requirementProfileDTO2.setUserId(1L);
        requirementProfileDTO2.setName("name");
        requirementProfileDTOList.add(requirementProfileDTO2);

        given(userACL.getUser()).willReturn(user);
        given(requirementProfileRepository.findRequirementProfileByUser(user, PageRequest.of(1,2))).willReturn(new PageImpl<>(requirementProfileDTOList));

        // When
        Page<RequirementProfileDTO> page = requirementProfileService.getListRequirementProfile(PageRequest.of(1,2));

        // Then
        assertThat(page.getContent().size()).isEqualTo(2);
        assertThat(page.getContent().get(1).getName()).isEqualTo("name");
    }
}
