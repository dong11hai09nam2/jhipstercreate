package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.enumeration.Gender;
import com.mycompany.myapp.repository.CandidateCriteriaRepository;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.service.dto.CandidateDTO;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import com.mycompany.myapp.service.error.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class CandidateMockTest {
    @InjectMocks
    CandidateService candidateService;

    @Mock
    UserACL userACL;

    @Mock
    CandidateRepository candidateRepository;

    @Mock
    CandidateCriteriaRepository candidateCriteriaRepository;

    /** ----------------------------------------------------- */
    /** ------------------- saveCandidate ------------------- */
    /** ----------------------------------------------------- */
    @Test
    public void testSaveCandidateWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> candidateService.saveCandidate(new CandidateDTO()));

        // THen
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testSaveCandidateWithCasePersonalNumberNull() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setPersonalNumber(null);

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.saveCandidate(candidateDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.personalNumberNull");
    }

    @Test
    public void testSaveCandidateWithCasePhoneNumberInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setPersonalNumber("21424124");
        candidateDTO.setPhoneNumber("adafgaddf2");

        // WHen
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.saveCandidate(candidateDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.phoneNumberInvalid");
    }

    @Test
    public void testSaveCandidateWithCaseEmailPrivateInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setPersonalNumber("2142421");
        candidateDTO.setPhoneNumber("0239482912");
        candidateDTO.setEmailPrivate("nam.gmail.com");

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.saveCandidate(candidateDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.emailPrivateInvalid");
    }

    @Test
    public void testSaveCandidateWithCaseEmailWorkInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setPersonalNumber("23124214214");
        candidateDTO.setPhoneNumber("0239471628");
        candidateDTO.setEmailPrivate("nam@gmail.com");
        candidateDTO.setEmailWork("nam.gmail.com");

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.saveCandidate(candidateDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.emailWorkInvalid");
    }

    @Test
    public void testSaveCandidateWithCaseBirthYearInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setPersonalNumber("241");
        candidateDTO.setPhoneNumber("0321439843");
        candidateDTO.setEmailPrivate("nam@gmail.com");
        candidateDTO.setEmailWork("name@gmail.com");
        candidateDTO.setBirthYear(-2);

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.saveCandidate(candidateDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.birthYearInvalid");
    }

    @Test
    public void testSaveCandidateWithCaseZipCodeInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setPersonalNumber("241");
        candidateDTO.setPhoneNumber("0321439843");
        candidateDTO.setEmailPrivate("nam@gmail.com");
        candidateDTO.setEmailWork("name@gmail.com");
        candidateDTO.setBirthYear(2010);
        candidateDTO.setZipCode(-2);

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.saveCandidate(candidateDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.zipCodeInvalid");
    }

    @Test
    public void testSaveCandidateSuccess() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setFirstName("John");
        candidateDTO.setLastName("Doe");
        candidateDTO.setBirthYear(1998);
        candidateDTO.setPersonalNumber("121-460-890-123");
        candidateDTO.setGender(Gender.Male);
        candidateDTO.setRegion("null");
        candidateDTO.setLinkLnLink("Null");
        candidateDTO.setEmployer("TechLead");
        candidateDTO.setFirstEmployment("FPT");
        candidateDTO.setManagerialPosition("Developer");
        candidateDTO.setBusinessAreas("ZenTower");
        candidateDTO.setStreetAddress("....");
        candidateDTO.setZipCode(3);
        candidateDTO.setCity("Ha noi");
        candidateDTO.setPhoneNumber("0432438253");
        candidateDTO.setEmailPrivate("nam@gmail.com");
        candidateDTO.setEmailWork("nam@Gmail.com");

        // When
        CandidateDTO result = candidateService.saveCandidate(candidateDTO);

        // Then
        assertThat(result.getZipCode()).isEqualTo(3);
    }

    /** ----------------------------------------------------- */
    /** ---------------- importCandidateList ---------------- */
    /** ----------------------------------------------------- */
    @Test
    public void testImportCandidateListWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // Then
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> candidateService.importCandidateList(null));

        // WHen
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testImportCandidateListWithCaseWrongFormat() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/banner.txt");

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.uploadExcelWrongFormat");
    }

    @Test
    public void testImportCandidateListWithCaseLastCellInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_last_cell_Invalid.xlsx");

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.lastCellInvalid");
    }

    @Test
    public void testImportCandidateListWithCaseFieldNullOrEmpty() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_field_null_or_empty.xlsx");

        // When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.cellContentError: 10");
    }

    @Test
    public void testImportCandidateListWithCasePersonalNumberInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_personal_number_invalid.xlsx");

        // When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.personalNumberInvalid");
    }

    @Test
    public void testImportCandidateListWithCaseZipCodeNegative() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_zip_code_negative.xlsx");

        // When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.zipCodeNegative");
    }

    @Test
    public void testImportCandidateListWithCasePhoneNumberInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_phone_Invalid.xlsx");

        // When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.phoneNumberInvalid");
    }

    @Test
    public void testImportCandidateListWithCaseEmailPrivateInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_email_invalid.xlsx");

        // When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.emailPrivateInvalid");
    }

    @Test
    public void testImportCandidateListWithCaseEmailWorkInvalid() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_email_work_invalid.xlsx");

        // When
        BadRequestException result = assertThrows(BadRequestException.class, () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.emailWorkInvalid");
    }

    @Test
    public void testImportCandidateListWithCaseUploadExcelDataEmpty() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_empty.xlsx");

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> candidateService.importCandidateList(new FileInputStream(file)));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.uploadExcelDataEmpty");
    }

    @Test
    public void testImportCandidateListSuccessful() throws IOException {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        File file = new File("src/main/resources/testing_excel_candidate_success.xlsx");

        // When
        List<CandidateDTO> result = candidateService.importCandidateList(new FileInputStream(file));

        // Then
        assertThat(result.get(9).getFirstName()).isEqualTo("John10");
    }

    /** ----------------------------------------------------- */
    /** ------------------- findCandidates ------------------ */
    /** ----------------------------------------------------- */
    @Test
    public void testFindCandidatesWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> candidateService.findCandidates(null));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testFindCandidatesWithSuccess() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Candidate candidate = new Candidate();
        candidate.setId(1L);

        given(candidateRepository.findCandidateByDeletedIsFalse(PageRequest.of(1,20))).willReturn(new PageImpl<>(Collections.singletonList(candidate)));

        // When
        Page<CandidateDTO> result = candidateService.findCandidates(PageRequest.of(1,20));

        // Then
        assertThat(result.getContent().get(0).getId()).isEqualTo(1);
    }

    /** ----------------------------------------------------- */
    /** -------------- findCandidatesByCriteria ------------- */
    /** ----------------------------------------------------- */
    @Test
    public void testFindCandidatesByCriteriaWithNotUser() {
        // Given
        given(userACL.isUser()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> candidateService.findCandidatesByCriteria(PageRequest.of(1,2), new CandidateDTO(), null));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }

    @Test
    public void testFindCandidatesByCriteriaSuccess() {
        // GIven
        given(userACL.isUser()).willReturn(true);

        String keyword = "a";
        CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setFirstName("yka-nb");

        Candidate candidate = new Candidate();
        candidate.setFirstName("kt-yka-nb");

        given(candidateCriteriaRepository.findCandidateCriteria(PageRequest.of(1,2), candidateDTO, keyword)).willReturn(new PageImpl<>(Collections.singletonList(candidate)));

        // When
        Page<CandidateDTO> result = candidateService.findCandidatesByCriteria(PageRequest.of(1,2), candidateDTO, keyword);

        // Then
        assertThat(result.getContent().get(0).getFirstName()).isEqualTo("kt-yka-nb");
    }
}
