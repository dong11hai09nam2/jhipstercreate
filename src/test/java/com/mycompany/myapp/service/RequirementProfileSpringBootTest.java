package com.mycompany.myapp.service;

import com.mycompany.myapp.JhipsterApp;
import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(classes = JhipsterApp.class)
public class RequirementProfileSpringBootTest {
    @Autowired
    UserACL userACL;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RequirementProfileRepository requirementProfileRepository;

    @BeforeEach
    public void init() {
        User user1 = new User();
        user1.setLogin("user1");
        user1.setPassword("123456789012345678901234567890123456789012345678901234567890");
        user1.setActivated(true);
        user1.setCreatedBy("admin");
        user1 = userRepository.save(user1);

        User user2 = new User();
        user2.setLogin("user2");
        user2.setPassword("123456789012345678901234567890123456789012345678901234567890");
        user2.setActivated(true);
        user2.setCreatedBy("admin");
        user2 = userRepository.save(user2);

        RequirementProfile requirementProfile1 = new RequirementProfile();
        requirementProfile1.setUser(user2);
        requirementProfile1.setName("name1");
        requirementProfileRepository.save(requirementProfile1);

        RequirementProfile requirementProfile2 = new RequirementProfile();
        requirementProfile2.setUser(user1);
        requirementProfile2.setName("name2");
        requirementProfileRepository.save(requirementProfile2);

        RequirementProfile requirementProfile3 = new RequirementProfile();
        requirementProfile3.setUser(user1);
        requirementProfile3.setName("name3");
        requirementProfileRepository.save(requirementProfile3);
    }

    @AfterEach
    public void destroy() {
        requirementProfileRepository.deleteAll();
    }

    @Test
    public void testGetListRequirementProfile() {
        Optional<User> user = userRepository.findOneByLogin("user1");
        Page<RequirementProfileDTO> page = requirementProfileRepository.findRequirementProfileByUser(user.get(), PageRequest.of(0,20));
        assertThat(page.getContent().size()).isEqualTo(2);
        assertThat(page.getContent().get(0).getName()).isEqualTo("name2");
    }
}
