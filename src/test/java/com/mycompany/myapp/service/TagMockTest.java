package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.domain.TagTracking;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.TagRepository;
import com.mycompany.myapp.repository.TagTrackingRepository;
import com.mycompany.myapp.service.dto.TagDTO;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import com.mycompany.myapp.service.error.BadRequestException;
import com.mycompany.myapp.service.error.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class TagMockTest {
    @InjectMocks
    TagService tagService;

    @Mock
    UserACL userACL;

    @Mock
    TagRepository tagRepository;

    @Mock
    TagTrackingService tagTrackingService;

    @Mock
    TagTrackingRepository tagTrackingRepository;

    /** ------------------------------------------------------- */
    /** ------------------------ save ------------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testSaveWithNonUser() {
        // Given
        given(userACL.isUser()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.save("3"));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notUser");
    }

    @Test
    public void testSaveWithCaseNewTag() {
        // Given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        given(userACL.getUser()).willReturn(user);
        given(tagRepository.findTagByValue("keyword")).willReturn(Optional.empty());

        // When
        TagDTO result = tagService.save("keyword");

        // Then
        assertThat(result.getCountSearch()).isEqualTo(1L);
        assertThat(result.getValue()).isEqualTo("keyword");
    }

    @Test
    public void testSaveWithCaseTagIsBlockTag() {
        // Given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        Tag tag = new Tag();
        tag.setValue("value");
        tag.setBlockTag(true);

        TagTracking tagTracking = new TagTracking();
        tagTracking.setTag(tag);

        given(userACL.getUser()).willReturn(user);
        given(tagRepository.findTagByValue("value")).willReturn(Optional.of(tag));

        // When
        TagDTO result = tagService.save("value");

        // Then
        assertThat(result.getValue()).isEqualTo("value");
    }

    @Test
    public void testSaveWithCaseTagExistedAndTagTrackingExisted() {
        // Given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        Tag tag = new Tag();
        tag.setValue("value");
        tag.setCountSearch(1);
        tag.setDeleted(false);

        TagTracking tagTracking = new TagTracking();
        tagTracking.setTag(tag);

        given(userACL.getUser()).willReturn(user);
        given(tagRepository.findTagByValue("value")).willReturn(Optional.of(tag));
        given(tagTrackingService.save(user, tag)).willReturn(null);

        // When
        TagDTO result = tagService.save("value");

        // Then
        assertThat(tag.getCountSearch()).isEqualTo(2);
        then(tagRepository).should(times(1)).save(tag);
    }

    @Test
    public void testSaveWithCaseCountTagIs5() {
        // Given
        given(userACL.isUser()).willReturn(true);

        User user = new User();
        user.setId(1L);

        Tag tag = new Tag();
        tag.setValue("value");
        tag.setCountSearch(4L);
        tag.setActive(false);
        tag.setDeleted(false);

        TagTracking tagTracking = new TagTracking();
        tagTracking.setTag(tag);

        given(userACL.getUser()).willReturn(user);
        given(tagRepository.findTagByValue("value")).willReturn(Optional.of(tag));
        given(tagTrackingService.save(user, tag)).willReturn(tagTracking);
        given(tagTrackingRepository.countTagTrackingByTag(tag)).willReturn(5L);

        // When
        tagService.save("value");

        // Then
        assertThat(tag.isActive()).isTrue();
        assertThat(tag.getCountSearch()).isEqualTo(5);
    }

    /** ------------------------------------------------------- */
    /** --------------------- getListTag ---------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testGetListTagWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.getListTag(PageRequest.of(0,20)));

        // THen
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    /** ------------------------------------------------------- */
    /** ------------------- saveWithAdmin --------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testSaveWithAdminWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.saveWithAdmin("like this"));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testSaveWithAdminTagExistedAndNotDeleted() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        String keyword = "this is keyword";

        Tag tag = new Tag();
        tag.setValue(keyword);
        tag.setDeleted(false);

        given(tagRepository.findTagByValue(keyword)).willReturn(Optional.of(tag));

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> tagService.saveWithAdmin(keyword));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.tagExisted");
    }

    @Test
    public void testSaveWithAdminCaseTagIsDeleted() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        String keyword = "asdjbauigwegb";

        Tag tag = new Tag();
        tag.setDeleted(true);
        tag.setValue(keyword);

        given(tagRepository.findTagByValue(keyword)).willReturn(Optional.of(tag));

        // When
        tagService.saveWithAdmin(keyword);

        // Then
        assertThat(tag.isDeleted()).isFalse();
    }

    @Test
    public void testSaveWithAdminSuccess() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        String keyword = "this is keyword";

        given(tagRepository.findTagByValue(keyword)).willReturn(Optional.empty());

        // When
        TagDTO result = tagService.saveWithAdmin(keyword);

        // Then
        assertThat(result.getValue()).isEqualTo(keyword);
    }

    /** ------------------------------------------------------- */
    /** ------------------- updateWithAdmin ------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testUpdateWithAdminWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // WHen
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.updateWithAdmin(new TagDTO()));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testUpdateWithAdminCaseNotFoundTag() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        TagDTO tagDTO = new TagDTO();
        tagDTO.setValue("value");
        tagDTO.setId(1L);

        given(tagRepository.findTagsByIdAndDeletedIsFalse(tagDTO.getId())).willReturn(Optional.empty());

        // When
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class,
            () -> tagService.updateWithAdmin(tagDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    @Test
    public void testUpdateWithAdminSuccess() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setValue("old value");
        tag.setId(1L);
        tag.setDeleted(false);
        tag.setActive(true);

        TagDTO tagDTO = new TagDTO();
        tagDTO.setId(1L);
        tagDTO.setValue("new value");

        given(tagRepository.findTagsByIdAndDeletedIsFalse(tagDTO.getId())).willReturn(Optional.of(tag));

        // WHen
        tagService.updateWithAdmin(tagDTO);

        // Then
        assertThat(tag.getValue()).isEqualTo("new value");
    }

    /** ------------------------------------------------------- */
    /** ---------------- deleteTagWithAdmin ------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testDeleteTagWithAdminCaseNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.deleteTagWithAdmin(1L));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testDeleteTagWithAdminCaseNotFound() {
        // Given
        given(userACL.isAdmin()).willReturn(true);
        given(tagRepository.findById(1L)).willReturn(Optional.empty());

        // WHen
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class,
            () -> tagService.deleteTagWithAdmin(1L));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    @Test
    public void testDeleteTagWithAdminSuccess() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setValue("value");
        tag.setActive(true);
        tag.setDeleted(false);

        given(tagRepository.findById(1L)).willReturn(Optional.of(tag));

        // When
        tagService.deleteTagWithAdmin(1L);

        // Then
        then(tagRepository).should().delete(tag);
    }

    /** ------------------------------------------------------- */
    /** --------------------- saveBlockTag -------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testSaveBlockTagWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        String value = "value";

        // WHen
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.saveBlockTag(value));

        // THen
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testSaveBlockTagCaseTagExisted() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setValue("value");
        tag.setDeleted(false);
        tag.setBlockTag(false);

        given(tagRepository.findTagByValue("value")).willReturn(Optional.of(tag));

        // When
        tagService.saveBlockTag("value");

        // Then
        assertThat(tag.isBlockTag()).isTrue();
    }

    @Test
    public void testSaveBlockTagCaseTagNotExisted() {
        // Given
        given(userACL.isAdmin()).willReturn(true);
        given(tagRepository.findTagByValue("value")).willReturn(Optional.empty());

        // When
        TagDTO result = tagService.saveBlockTag("value");

        // Then
        assertThat(result.getValue()).isEqualTo("value");
    }

    /** ------------------------------------------------------- */
    /** ------------------- updateBlockTag -------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testUpdateBlockTagWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // WHen
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.updateBlockTag(new TagDTO()));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testUpdateBlockTagCaseNotFound() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        TagDTO tagDTO = new TagDTO();
        tagDTO.setId(1L);
        tagDTO.setValue("value");

        given(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(1L)).willReturn(Optional.empty());

        // When
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class,
            () -> tagService.updateBlockTag(tagDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    @Test
    public void testUpdateBlockTagCaseValueTagDTONullOrSameValueTag() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        TagDTO tagDTO = new TagDTO();
        tagDTO.setId(1L);

        Tag tag = new Tag();
        tag.setValue("value");
        tag.setBlockTag(true);
        tag.setDeleted(false);
        tag.setId(1L);

        given(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(1L)).willReturn(Optional.of(tag));

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> tagService.updateBlockTag(tagDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.valueTagDTONullOrSameValueTag");
    }

    @Test
    public void testUpdateBlockTagCaseValueIsExisted() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        TagDTO tagDTO = new TagDTO();
        tagDTO.setId(1L);
        tagDTO.setValue("value2");

        Tag tag = new Tag();
        tag.setValue("value");
        tag.setBlockTag(true);
        tag.setDeleted(false);
        tag.setId(1L);

        Tag tag2 = new Tag();
        tag2.setValue("value");
        tag2.setBlockTag(false);
        tag2.setDeleted(false);
        tag2.setId(2L);

        given(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(1L)).willReturn(Optional.of(tag));
        given(tagRepository.findTagByValue(tagDTO.getValue())).willReturn(Optional.of(tag2));

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> tagService.updateBlockTag(tagDTO));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.valueIsExisted");
    }

    /** ------------------------------------------------------- */
    /** ------------------- deleteBlockTag -------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testDeleteBlockTagWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.deleteBlockTag(1L));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testDeleteBlockTagCaseNotFound() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        given(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(1L)).willReturn(Optional.empty());

        // When
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class,
            () -> tagService.deleteBlockTag(1L));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    @Test
    public void testDeleteBlockTagSuccess() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setDeleted(false);
        tag.setId(1L);

        given(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(1L)).willReturn(Optional.of(tag));

        // When
        tagService.deleteBlockTag(1L);

        // Then
        assertThat(tag.isDeleted()).isTrue();
    }

    /** ------------------------------------------------------- */
    /** ------------------ switchTagToBlock ------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testSwitchTagToBlockWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.switchTagToBlock("va"));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testSwitchTagToBlockCaseNotFound() {
        // Given
        given(userACL.isAdmin()).willReturn(true);
        given(tagRepository.findTagByValue("va")).willReturn(Optional.empty());

        // When
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class,
            () -> tagService.switchTagToBlock("va"));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    @Test
    public void testSwitchTagToBlockCaseTagIsBlock() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setBlockTag(true);
        tag.setValue("va");

        given(tagRepository.findTagByValue("va")).willReturn(Optional.of(tag));

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> tagService.switchTagToBlock("va"));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.tagIsBlock");
    }

    @Test
    public void testSwitchTagToBlockSuccess() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setBlockTag(false);
        tag.setValue("value");

        given(tagRepository.findTagByValue("value")).willReturn(Optional.of(tag));

        // When
        tagService.switchTagToBlock("value");

        // Then
        assertThat(tag.isBlockTag()).isTrue();
    }

    /** ------------------------------------------------------- */
    /** ------------------ switchTagToBlock ------------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testSwitchBlockToTagWithNonAdmin() {
        // Given
        given(userACL.isAdmin()).willReturn(false);

        // When
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> tagService.switchBlockToTag(1L));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testSwitchBlocktoTagWithCaseNotFound() {
        // Given
        given(userACL.isAdmin()).willReturn(true);
        given(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(1L)).willReturn(Optional.empty());

        // When
        ResourceNotFoundException result = assertThrows(ResourceNotFoundException.class,
            () -> tagService.switchBlockToTag(1L));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notFound");
    }

    @Test
    public void testSwitchBlockToTagWithCaseTagIsNotBlock() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setBlockTag(false);

        given(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(1L)).willReturn(Optional.of(tag));

        // When
        BadRequestException result = assertThrows(BadRequestException.class,
            () -> tagService.switchBlockToTag(1L));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.tagIsNotBlock");
    }

    @Test
    public void testSwitchBlockToTagSuccess() {
        // Given
        given(userACL.isAdmin()).willReturn(true);

        Tag tag = new Tag();
        tag.setBlockTag(true);
        tag.setId(1L);

        given(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(1L)).willReturn(Optional.of(tag));

        // When
        tagService.switchBlockToTag(1L);

        // Then
        assertThat(tag.isBlockTag()).isFalse();
    }

    /** ------------------------------------------------------- */
    /** --------------- saveTagsWithReadingTag ---------------- */
    /** ------------------------------------------------------- */
    @Test
    public void testSaveTagsWithReadingTag() {
        // Given
        List<String> stringList = new ArrayList<>();
        stringList.add("tag1");
        stringList.add("tag2");
        stringList.add("tag3");

        Tag tag1 = new Tag();
        tag1.setValue("tag1");
        tag1.setDeleted(true);
        tag1.setBlockTag(false);
        tag1.setActive(false);
        tag1.setCountSearch(5L);

        Tag tag2 = new Tag();
        tag2.setValue("tag2");
        tag2.setDeleted(false);
        tag2.setBlockTag(true);
        tag2.setActive(true);
        tag2.setCountSearch(5L);

        Tag tag3 = new Tag();
        tag3.setValue("tag3");

        given(tagRepository.findTagByValue("tag1")).willReturn(Optional.of(tag1));
        given(tagRepository.findTagByValue("tag2")).willReturn(Optional.of(tag2));
        given(tagRepository.findTagByValue("tag3")).willReturn(Optional.empty());

        // When
        tagService.saveTagsWithReadingTag(stringList);

        // Then
        assertThat(tag1.getCountSearch()).isEqualTo(0L);
        assertThat(tag1.isActive()).isTrue();
        assertThat(tag3.isDeleted()).isFalse();
    }
}
