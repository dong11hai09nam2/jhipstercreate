package com.mycompany.myapp.service;

import com.mycompany.myapp.repository.CvsRepository;
import com.mycompany.myapp.service.dto.CvsDTO;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class CvsMockTest {
    @InjectMocks
    CvsService cvsService;

    @Mock
    UserACL userACL;

    @Mock
    CvsRepository cvsRepository;

    @Mock
    TagService tagService;

    /** ----------------------------------------------------- */
    /** ----------------------- save ------------------------ */
    /** ----------------------------------------------------- */
    @Test
    public void testSaveWithNonAdmin() throws IOException {
        // Given
        given(userACL.isAdmin()).willReturn(false);
        File file = new File("src/main/resources/DongHaiNam-Test.pdf");
        MockMultipartFile mockMultipartFile = new MockMultipartFile("name", new FileInputStream(file));

        // WHen
        AccessForbiddenException result = assertThrows(AccessForbiddenException.class,
            () -> cvsService.save(mockMultipartFile));

        // Then
        assertThat(result.getMessage()).isEqualTo("error.notAdmin");
    }

    @Test
    public void testSaveSuccess() throws IOException {
        // Given
        given(userACL.isAdmin()).willReturn(true);
        File file = new File("src/main/resources/DongHaiNam-Test.pdf");
        MockMultipartFile mockMultipartFile = new MockMultipartFile("name", "DongHaiNam-Test.pdf","contentType", new FileInputStream(file));

        // When
        List<CvsDTO> result = cvsService.save(mockMultipartFile);

        // THen
        assertThat(result.get(0).getName()).isEqualTo("DongHaiNam-Test");
        assertThat(result.get(0).getContent().substring(0,10)).isEqualTo("MỤC TIÊU N");
    }
}
