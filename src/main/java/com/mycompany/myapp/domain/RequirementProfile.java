package com.mycompany.myapp.domain;

import com.mycompany.myapp.domain.enumeration.Gender;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;

import javax.persistence.*;

@Entity
@Table(name = "requirement_profile")
public class RequirementProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "year_of_birth")
    private int birthYear;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "first_employment")
    private String firstEmployment;

    @Column(name = "managerial_position")
    private String managerialPosition;

    @Column(name = "street_address")
    private String streetAddress;

    private String city;

    private boolean deleted;

    private String employer;

    private String region;

    private String country;

    public RequirementProfile() {}

    public RequirementProfile(RequirementProfileDTO requirementProfileDTO) {
        this.name = requirementProfileDTO.getName();
        this.firstName = requirementProfileDTO.getFirstName();
        this.lastName = requirementProfileDTO.getLastName();
        this.birthYear = requirementProfileDTO.getBirthYear();
        this.gender = requirementProfileDTO.getGender();
        this.firstEmployment = requirementProfileDTO.getFirstEmployment();
        this.managerialPosition = requirementProfileDTO.getManagerialPosition();
        this.streetAddress = requirementProfileDTO.getStreetAddress();
        this.city = requirementProfileDTO.getCity();
        this.employer = requirementProfileDTO.getEmployer();
        this.region = requirementProfileDTO.getRegion();
        this.country = requirementProfileDTO.getCountry();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(String firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public String getManagerialPosition() {
        return managerialPosition;
    }

    public void setManagerialPosition(String managerialPosition) {
        this.managerialPosition = managerialPosition;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
