package com.mycompany.myapp.domain;

import javax.persistence.*;

@Entity
@Table(name = "tag")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "value")
    private String value;

    @Column(name = "count_search")
    private long countSearch;

    @Column(name = "block_tag")
    private boolean blockTag;

    private boolean active;

    private boolean deleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getCountSearch() {
        return countSearch;
    }

    public void setCountSearch(long countSearch) {
        this.countSearch = countSearch;
    }

    public boolean isBlockTag() {
        return blockTag;
    }

    public void setBlockTag(boolean blockTag) {
        this.blockTag = blockTag;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
