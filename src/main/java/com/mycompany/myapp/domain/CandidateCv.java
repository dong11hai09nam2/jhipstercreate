package com.mycompany.myapp.domain;

import javax.persistence.*;

@Entity
@Table(name = "candidate_cv")
public class CandidateCv {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cvs_id")
    private Cvs cvs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public Cvs getCvs() {
        return cvs;
    }

    public void setCvs(Cvs cvs) {
        this.cvs = cvs;
    }
}
