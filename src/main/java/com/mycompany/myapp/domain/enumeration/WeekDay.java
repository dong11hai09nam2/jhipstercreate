package com.mycompany.myapp.domain.enumeration;

public enum WeekDay {
    MON, TUE, WED, THU, FRI, SAT, SUN
}
