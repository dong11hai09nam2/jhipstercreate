package com.mycompany.myapp.domain;

import com.mycompany.myapp.domain.enumeration.Gender;
import com.mycompany.myapp.service.dto.CandidateDTO;

import javax.persistence.*;

@Entity
@Table(name = "candidate")
public class Candidate extends AbstractAuditingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "year_of_birth")
    private int birthYear;

    @Column(name = "personal_number")
    private String personalNumber;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "linkedln_link")
    private String linkLnLink;

    @Column(name = "first_employment")
    private String firstEmployment;

    @Column(name = "managerial_position")
    private String managerialPosition;

    @Column(name = "business_areas")
    private String businessAreas;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "zip_code")
    private int zipCode;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email_private")
    private String emailPrivate;

    @Column(name = "email_work")
    private String emailWork;

    private String city;

    private boolean deleted;

    private String employer;

    private String region;

    private String country;

    public Candidate(CandidateDTO candidateDTO) {
        this.id = candidateDTO.getId();
        this.firstName = candidateDTO.getFirstName();
        this.lastName = candidateDTO.getLastName();
        this.birthYear = candidateDTO.getBirthYear();
        this.personalNumber = candidateDTO.getPersonalNumber();
        this.gender = candidateDTO.getGender();
        this.region = candidateDTO.getRegion();
        this.country = candidateDTO.getCountry();
        this.linkLnLink = candidateDTO.getLinkLnLink();
        this.employer = candidateDTO.getEmployer();
        this.firstEmployment = candidateDTO.getFirstEmployment();
        this.managerialPosition = candidateDTO.getManagerialPosition();
        this.businessAreas = candidateDTO.getBusinessAreas();
        this.streetAddress = candidateDTO.getStreetAddress();
        this.zipCode = candidateDTO.getZipCode();
        this.city = candidateDTO.getCity();
        this.phoneNumber = candidateDTO.getPhoneNumber();
        this.emailPrivate = candidateDTO.getEmailPrivate();
        this.emailWork = candidateDTO.getEmailWork();
    }

    public Candidate() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getLinkLnLink() {
        return linkLnLink;
    }

    public void setLinkLnLink(String linkLnLink) {
        this.linkLnLink = linkLnLink;
    }

    public String getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(String firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public String getManagerialPosition() {
        return managerialPosition;
    }

    public void setManagerialPosition(String managerialPosition) {
        this.managerialPosition = managerialPosition;
    }

    public String getBusinessAreas() {
        return businessAreas;
    }

    public void setBusinessAreas(String businessAreas) {
        this.businessAreas = businessAreas;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(String emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getEmailWork() {
        return emailWork;
    }

    public void setEmailWork(String emailWork) {
        this.emailWork = emailWork;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
