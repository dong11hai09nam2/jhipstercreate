package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.RequirementProfileRepository;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import com.mycompany.myapp.service.utils.Utils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class RequirementProfileService {

    private UserACL userACL;

    private UserRepository userRepository;

    private RequirementProfileRepository requirementProfileRepository;

    public RequirementProfileService(UserACL userACL,
                                     UserRepository userRepository,
                                     RequirementProfileRepository requirementProfileRepository) {
        this.userACL = userACL;
        this.userRepository = userRepository;
        this.requirementProfileRepository = requirementProfileRepository;
    }

    public RequirementProfileDTO saveRequirementProfile(RequirementProfileDTO requirementProfileDTO) {
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }
        User user = userACL.getUser();
        RequirementProfile requirementProfile = new RequirementProfile(requirementProfileDTO);
        requirementProfile.setUser(user);
        requirementProfileRepository.save(requirementProfile);
        return new RequirementProfileDTO(requirementProfile);
    }

    public RequirementProfileDTO updateRequirementProfile(RequirementProfileDTO requirementProfileDTO) {
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }

        RequirementProfile requirementProfile = Utils.requireExists(requirementProfileRepository.findById(requirementProfileDTO.getId()), "error.notFound");
        User user = userACL.getUser();
        userACL.isCustomerOwner(user.getId(), requirementProfile.getUser().getId());
        if (requirementProfileDTO.getName() != null) {
            requirementProfile.setName(requirementProfileDTO.getName());
        }
        if (requirementProfileDTO.getFirstName() != null) {
            requirementProfile.setFirstName(requirementProfileDTO.getFirstName());
        }
        if (requirementProfileDTO.getLastName() != null) {
            requirementProfile.setLastName(requirementProfileDTO.getLastName());
        }
        if (requirementProfileDTO.getBirthYear() != 0) {
            requirementProfile.setBirthYear(requirementProfileDTO.getBirthYear());
        }
        if (requirementProfileDTO.getGender() != null) {
            requirementProfile.setGender(requirementProfileDTO.getGender());
        }
        if (requirementProfileDTO.getFirstEmployment() != null) {
            requirementProfile.setFirstEmployment(requirementProfileDTO.getFirstEmployment());
        }
        if (requirementProfileDTO.getManagerialPosition() != null) {
            requirementProfile.setManagerialPosition(requirementProfileDTO.getManagerialPosition());
        }
        if (requirementProfileDTO.getStreetAddress() != null) {
            requirementProfile.setStreetAddress(requirementProfileDTO.getStreetAddress());
        }
        if (requirementProfileDTO.getCity() != null) {
            requirementProfile.setCity(requirementProfileDTO.getCity());
        }
        if (requirementProfileDTO.getEmployer() != null) {
            requirementProfile.setEmployer(requirementProfileDTO.getEmployer());
        }
        if (requirementProfileDTO.getRegion() != null) {
            requirementProfile.setRegion(requirementProfileDTO.getRegion());
        }
        if (requirementProfileDTO.getCountry() != null) {
            requirementProfile.setCountry(requirementProfileDTO.getCountry());
        }
        requirementProfileRepository.save(requirementProfile);
        return new RequirementProfileDTO(requirementProfile);
    }

    public void deleteRequirementProfile(Long id) {
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }

        User user = userACL.getUser();
        RequirementProfile requirementProfile = Utils.requireExists(requirementProfileRepository.findById(id), "error.notFound");
        userACL.isCustomerOwner(user.getId(), requirementProfile.getUser().getId());
        requirementProfile.setDeleted(true);
        requirementProfileRepository.save(requirementProfile);
    }

    public Page<RequirementProfileDTO> getListRequirementProfile(Pageable pageable) {
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }
        User user = userACL.getUser();
        Page<RequirementProfileDTO> page = requirementProfileRepository.findRequirementProfileByUser(user, pageable);
        return page;
    }
}
