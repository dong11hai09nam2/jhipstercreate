package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.security.SecurityUtils;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import com.mycompany.myapp.service.utils.Utils;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserACL {

    private final UserRepository userRepository;

    public UserACL(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Check if current user is admin
     * @return true if user is logged in and has role 'admin', false otherwise
     */
    public boolean isAdmin() {
        return SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN);
    }

    public boolean isUser(){
        return SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.USER);
    }

    public User getUser(){
        String userLogin = Utils.requireExists(SecurityUtils.getCurrentUserLogin(), "error.notFoundUserLogin");
        User user = Utils.requireExists(userRepository.findOneByLogin(userLogin), "error.notFoundUser");
        return user;
    }

    public void isCustomerOwner(Long idFirst, Long idSecond){
        if (!Objects.equals(idFirst, idSecond)){
            throw new AccessForbiddenException("error.notUserOfRequirementProfile");
        }
    }

}
