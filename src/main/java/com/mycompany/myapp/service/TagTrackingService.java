package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.domain.TagTracking;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.TagTrackingRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TagTrackingService {
    private final UserACL userACL;
    private final TagTrackingRepository tagTrackingRepository;

    public TagTrackingService(UserACL userACL, TagTrackingRepository tagTrackingRepository) {
        this.userACL = userACL;
        this.tagTrackingRepository = tagTrackingRepository;
    }

    public TagTracking save(User user, Tag tag) {
        Optional<TagTracking> tagTracking = tagTrackingRepository.findTagTrackingByUserAndTag(user, tag);
        if (tagTracking.isPresent()) {
            return null;
        } else {
            TagTracking tagTracking1 = new TagTracking();
            tagTracking1.setUser(user);
            tagTracking1.setTag(tag);
            tagTrackingRepository.save(tagTracking1);
            return tagTracking1;
        }
    }
}
