package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.domain.TagTracking;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.TagRepository;
import com.mycompany.myapp.repository.TagTrackingRepository;
import com.mycompany.myapp.service.dto.TagDTO;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import com.mycompany.myapp.service.error.BadRequestException;
import com.mycompany.myapp.service.utils.Utils;
import liquibase.repackaged.org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class TagService {
    private final UserACL userACL;

    private final TagRepository tagRepository;

    private final TagTrackingService tagTrackingService;

    private final TagTrackingRepository tagTrackingRepository;

    public TagService(UserACL userACL, TagRepository tagRepository, TagTrackingService tagTrackingService, TagTrackingRepository tagTrackingRepository) {
        this.userACL = userACL;
        this.tagRepository = tagRepository;
        this.tagTrackingService = tagTrackingService;
        this.tagTrackingRepository = tagTrackingRepository;
    }

    public TagDTO save(String keyword) {
        if (!userACL.isUser()) {
            throw new AccessForbiddenException("error.notUser");
        }
        User user = userACL.getUser();
        /* RD-1038: Add service to count as soon as a tag is searched by customer */
        Optional<Tag> tagOptional = tagRepository.findTagByValue(keyword);
        Tag tag = new Tag();
        /* RD-1038: Add service to count as soon as a tag is searched by customer */
        if (tagOptional.isPresent()) {
            tag = tagOptional.get();
            if (!tag.isBlockTag()) {
                TagTracking tagTracking = tagTrackingService.save(user, tag);
                if (!tag.isDeleted()) {
                    if (tagTracking != null) {
                        if (!tag.isActive()) {
                            if (tagTrackingRepository.countTagTrackingByTag(tag) >= 5) {
                                tag.setActive(true);
                            }
                        }
                    }
                    tag.setCountSearch(tag.getCountSearch() + 1);
                    tagRepository.save(tag);
                }
            }
            /* RD-1038: Add service to count as soon as a tag is searched by customer */
        } else {
            tag.setValue(keyword);
            tag.setActive(false);
            tag.setCountSearch(1L);
            tagRepository.save(tag);
            tagTrackingService.save(user, tag);
        }
        return new TagDTO(tag);
    }

    public Page<TagDTO> getListTag(Pageable pageable) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        return tagRepository.findTagsByDeletedIsFalseAndActiveIsTrue(pageable);
    }

    public TagDTO saveWithAdmin(String keyword) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Optional<Tag> tagOptional = tagRepository.findTagByValue(keyword);
        Tag tag = new Tag();
        if (tagOptional.isPresent()) {
            if (!tagOptional.get().isDeleted()) {
                throw new BadRequestException("error.tagExisted", null);
            } else {
                tag = tagOptional.get();
                tag.setDeleted(false);
            }
        } else {
            tag.setValue(keyword);
            tag.setActive(true);
            tag.setDeleted(false);
            tag.setCountSearch(0L);
        }
        tagRepository.save(tag);
        return new TagDTO(tag);
    }

    public TagDTO updateWithAdmin(TagDTO tagDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findTagsByIdAndDeletedIsFalse(tagDTO.getId()), "error.notFound");
        if (tagDTO.getValue().equals(tag.getValue())) {
            return null;
        }
        tag.setValue(tagDTO.getValue());
        tagRepository.save(tag);
        return new TagDTO(tag);
    }

    public void deleteTagWithAdmin(Long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findById(id), "error.notFound");
        tagRepository.delete(tag);
    }

    public TagDTO saveBlockTag(String value) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Optional<Tag> tagOptional = tagRepository.findTagByValue(value);
        Tag tag = new Tag();
        if (tagOptional.isPresent()) {
            tag = tagOptional.get();
            if (!tag.isBlockTag()) {
                tag.setBlockTag(true);
            }
        } else {
            tag.setActive(false);
            tag.setBlockTag(true);
            tag.setCountSearch(0L);
            tag.setDeleted(false);
            tag.setValue(value);
        }
        tagRepository.save(tag);
        return new TagDTO(tag);
    }

    public TagDTO updateBlockTag(TagDTO tagDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(tagDTO.getId()), "error.notFound");
        Optional<Tag> tagOptional = tagRepository.findTagByValue(tagDTO.getValue());
        if (!StringUtils.isBlank(tagDTO.getValue()) && !Objects.equals(tag.getValue(), tagDTO.getValue())) {
            tag.setValue(tagDTO.getValue());
        } else {
            throw new BadRequestException("error.valueTagDTONullOrSameValueTag", null);
        }
        if (tagOptional.isPresent()) {
            throw new BadRequestException("error.valueIsExisted", null);
        }
        tagRepository.save(tag);
        return new TagDTO(tag);
    }

    public void deleteBlockTag(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(id), "error.notFound");
        tag.setDeleted(true);
        tagRepository.save(tag);
    }

    public TagDTO switchTagToBlock(String value) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findTagByValue(value), "error.notFound");
        if (tag.isBlockTag()) {
            throw new BadRequestException("error.tagIsBlock", null);
        }
        tag.setBlockTag(true);
        tagRepository.save(tag);
        return new TagDTO(tag);
    }

    public TagDTO switchBlockToTag(long id) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Tag tag = Utils.requireExists(tagRepository.findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(id), "error.notFound");
        if (!tag.isBlockTag()) {
            throw new BadRequestException("error.tagIsNotBlock", null);
        }
        tag.setBlockTag(false);
        tagRepository.save(tag);
        return new TagDTO(tag);
    }

    public void saveTagsWithReadingTag(List<String> stringList) {
        for (String value : stringList) {
            Optional<Tag> tagOptional = tagRepository.findTagByValue(value);
            Tag tag = new Tag();
            if (tagOptional.isPresent()) {
                tag = tagOptional.get();
                if (!tag.isBlockTag()) {
                    if (tag.isDeleted()) {
                        tag.setDeleted(false);
                        tag.setCountSearch(0L);
                    }
                    if (!tag.isActive()) {
                        tag.setActive(true);
                    }
                }
            } else {
                tag.setValue(value);
                tag.setDeleted(false);
                tag.setBlockTag(false);
                tag.setCountSearch(0L);
                tag.setActive(true);
            }
            tagRepository.save(tag);
        }
    }
}
