package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.enumeration.Gender;

import java.util.List;

public class CandidateDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private int birthYear;
    private String personalNumber;
    private Gender gender;
    private String linkLnLink;
    private String firstEmployment;
    private String managerialPosition;
    private String businessAreas;
    private String streetAddress;
    private int zipCode;
    private String phoneNumber;
    private String emailPrivate;
    private String emailWork;
    private String city;
    private String employer;
    private String region;
    private String country;
    private List<CvsDTO> cvsDTOList;

    public CandidateDTO() {
    }

//    public CandidateDTO(RequirementProfile requirementProfile)

    public CandidateDTO(Candidate candidate) {
        this.id = candidate.getId();
        this.firstName = candidate.getFirstName();
        this.lastName = candidate.getLastName();
        this.birthYear = candidate.getBirthYear();
        this.personalNumber = candidate.getPersonalNumber();
        this.gender = candidate.getGender();
        this.linkLnLink = candidate.getLinkLnLink();
        this.firstEmployment = candidate.getFirstEmployment();
        this.managerialPosition = candidate.getManagerialPosition();
        this.businessAreas = candidate.getBusinessAreas();
        this.streetAddress = candidate.getStreetAddress();
        this.zipCode = candidate.getZipCode();
        this.phoneNumber = candidate.getPhoneNumber();
        this.emailPrivate = candidate.getEmailPrivate();
        this.emailWork = candidate.getEmailWork();
        this.city = candidate.getCity();
        this.employer = candidate.getEmployer();
        this.region = candidate.getRegion();
        this.country = candidate.getCountry();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getLinkLnLink() {
        return linkLnLink;
    }

    public void setLinkLnLink(String linkLnLink) {
        this.linkLnLink = linkLnLink;
    }

    public String getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(String firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public String getManagerialPosition() {
        return managerialPosition;
    }

    public void setManagerialPosition(String managerialPosition) {
        this.managerialPosition = managerialPosition;
    }

    public String getBusinessAreas() {
        return businessAreas;
    }

    public void setBusinessAreas(String businessAreas) {
        this.businessAreas = businessAreas;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(String emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getEmailWork() {
        return emailWork;
    }

    public void setEmailWork(String emailWork) {
        this.emailWork = emailWork;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<CvsDTO> getCvsDTOList() {
        return cvsDTOList;
    }

    public void setCvsDTOList(List<CvsDTO> cvsDTOList) {
        this.cvsDTOList = cvsDTOList;
    }
}
