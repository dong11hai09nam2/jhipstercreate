package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.Cvs;

public class CvsDTO {
    private Long id;
    private String name;
    private String content;

    public CvsDTO(Cvs cvs) {
        this.id = cvs.getId();
        this.name = cvs.getName();
        this.content = cvs.getContent();
    }

    public CvsDTO() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
