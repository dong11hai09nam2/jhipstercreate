package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.domain.enumeration.Gender;

public class RequirementProfileDTO {
    private Long id;
    private String name;
    private Long userId;
    private String firstName;
    private String lastName;
    private int birthYear;
    private Gender gender;
    private String firstEmployment;
    private String managerialPosition;
    private String streetAddress;
    private String city;
    private String employer;
    private String region;
    private String country;

    private int zipCode;
    private String phoneNumber;
    private String emailPrivate;
    private String emailWork;

    public RequirementProfileDTO() {}

    public RequirementProfileDTO(RequirementProfile requirementProfile) {
        this.id = requirementProfile.getId();
        this.name = requirementProfile.getName();
        this.firstName = requirementProfile.getFirstName();
        this.lastName = requirementProfile.getLastName();
        this.birthYear = requirementProfile.getBirthYear();
        this.gender = requirementProfile.getGender();
        this.firstEmployment = requirementProfile.getFirstEmployment();
        this.managerialPosition = requirementProfile.getManagerialPosition();
        this.streetAddress = requirementProfile.getStreetAddress();
        this.city = requirementProfile.getCity();
        this.employer = requirementProfile.getEmployer();
        this.region = requirementProfile.getRegion();
        this.country = requirementProfile.getCountry();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getFirstEmployment() {
        return firstEmployment;
    }

    public void setFirstEmployment(String firstEmployment) {
        this.firstEmployment = firstEmployment;
    }

    public String getManagerialPosition() {
        return managerialPosition;
    }

    public void setManagerialPosition(String managerialPosition) {
        this.managerialPosition = managerialPosition;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailPrivate() {
        return emailPrivate;
    }

    public void setEmailPrivate(String emailPrivate) {
        this.emailPrivate = emailPrivate;
    }

    public String getEmailWork() {
        return emailWork;
    }

    public void setEmailWork(String emailWork) {
        this.emailWork = emailWork;
    }
}
