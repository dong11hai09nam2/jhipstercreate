package com.mycompany.myapp.service.dto;

import com.mycompany.myapp.domain.Tag;

public class TagDTO {
    private Long id;
    private String value;
    private long countSearch;

    public TagDTO(Tag tag) {
        this.id = tag.getId();
        this.value = tag.getValue();
        this.countSearch = tag.getCountSearch();
    }

    public TagDTO() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getCountSearch() {
        return countSearch;
    }

    public void setCountSearch(long countSearch) {
        this.countSearch = countSearch;
    }
}
