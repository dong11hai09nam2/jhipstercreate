package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.CandidateCv;
import com.mycompany.myapp.domain.Cvs;
import com.mycompany.myapp.domain.enumeration.Gender;
import com.mycompany.myapp.repository.CandidateCriteriaRepository;
import com.mycompany.myapp.repository.CandidateCvRepository;
import com.mycompany.myapp.repository.CandidateRepository;
import com.mycompany.myapp.repository.CvsRepository;
import com.mycompany.myapp.service.dto.CandidateDTO;
import com.mycompany.myapp.service.dto.CvsDTO;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import com.mycompany.myapp.service.error.BadRequestException;
import com.mycompany.myapp.service.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.apache.commons.collections4.IterableUtils.isEmpty;

@Service
public class CandidateService {
    private final UserACL userACL;

    private final CandidateRepository candidateRepository;

    private final CandidateCriteriaRepository candidateCriteriaRepository;

    private final TagService tagService;

    private final CandidateCvRepository candidateCvRepository;

    private final CvsRepository cvsRepository;

    public CandidateService(UserACL userACL, CandidateRepository candidateRepository, CandidateCriteriaRepository candidateCriteriaRepository, TagService tagService, CandidateCvRepository candidateCvRepository, CvsRepository cvsRepository) {
        this.userACL = userACL;
        this.candidateRepository = candidateRepository;
        this.candidateCriteriaRepository = candidateCriteriaRepository;
        this.tagService = tagService;
        this.candidateCvRepository = candidateCvRepository;
        this.cvsRepository = cvsRepository;
    }

    public CandidateDTO saveCandidate(CandidateDTO candidateDTO) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Candidate candidate = new Candidate(candidateDTO);
        if (candidate.getPersonalNumber() == null) {
            throw new BadRequestException("error.personalNumberNull", null);
        }
        String personalNumberPattern = "^\\d{3}-\\d{3}-\\d{3}-\\d{3}$";
        if (candidate.getPersonalNumber().length() == 0) {
            candidate.setPersonalNumber("");
        } else if (!Pattern.matches(personalNumberPattern, candidate.getPersonalNumber())) {
            throw new BadRequestException("error.personalNumberInvalid", null);
        } else {
            String personalNumber = candidateDTO.getPersonalNumber().trim().replaceAll(" ", "").replaceAll("-", "");
            candidate.setPersonalNumber(personalNumber);
        }
        String phoneNumberPattern = "^\\d{10,12}$";
        if (!Pattern.matches(phoneNumberPattern, candidateDTO.getPhoneNumber())) {
            throw new BadRequestException("error.phoneNumberInvalid", null);
        }
        if (!Utils.isValidEmail(candidateDTO.getEmailPrivate())) {
            throw new BadRequestException("error.emailPrivateInvalid", null);
        }
        if (!Utils.isValidEmail(candidateDTO.getEmailWork())) {
            throw new BadRequestException("error.emailWorkInvalid", null);
        }
        if (candidateDTO.getBirthYear() <= 0){
            throw new BadRequestException("error.birthYearInvalid", null);
        }
        if (candidateDTO.getZipCode() <= 0){
            throw new BadRequestException("error.zipCodeInvalid", null);
        }
        candidateRepository.save(candidate);
        return new CandidateDTO(candidate);
    }

    public List<CandidateDTO> importCandidateList(InputStream inputStream) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<Candidate> listCandidate = readExcelFileCandidateList(inputStream);

        if (listCandidate.size() > 0) {
            candidateRepository.saveAll(listCandidate);
            return listCandidate.stream().map(CandidateDTO::new).collect(Collectors.toList());
        } else {
            throw new BadRequestException("error.uploadExcelDataEmpty", null);
        }
    }

    private List<Candidate> readExcelFileCandidateList(InputStream inputStream){
        List<Candidate> candidateListToAdd = new ArrayList<>();
        try (Workbook workbook = WorkbookFactory.create(inputStream)) {
            //get sheet
            Sheet sheet = workbook.getSheetAt(0);

            //remove empty rows in Excel
            for (int i = 0; i < sheet.getLastRowNum(); i++) {
                if (isEmpty(sheet.getRow(i))) {
                    sheet.shiftRows(i + 1, sheet.getLastRowNum(), -1);
                    break;
                }
            }

            //Get add rows
            for (Row row : sheet) {
                if (row.getRowNum() == 0) {
                    //Ignore header
                    continue;
                }

                // Validation of cell
                Cell cellStart = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                if (cellStart.getCellType().equals(CellType._NONE) || cellStart.getCellType().equals(CellType.BLANK)) {
                    continue;
                }
                Cell cellEnd = row.getCell(16, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                boolean isExitContentAtLastCell = !cellEnd.getCellType().equals(CellType.STRING);
                if (isExitContentAtLastCell) {
                    throw new BadRequestException("error.lastCellInvalid", null);
                }

                // Get all cells
                Iterator<Cell> cellIterator = row.cellIterator();

                //Read cells and set value for book object
                Candidate candidate = new Candidate();
                while (cellIterator.hasNext()) {

                    Cell cell = cellIterator.next();
                    Object cellValue = getCellValue(cell);

                    // This paragraph may be redundant
                    // If cell value null or blank
                    if (cellValue == null || StringUtils.isBlank(cellValue.toString())) {
                        throw new BadRequestException("error.fieldNullOrEmpty", null);
                    }
                    // Set value for book object
                    int columnIndex = cell.getColumnIndex();
                    switch (columnIndex) {
                        case 0: {
                            candidate.setFirstName(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 1: {
                            candidate.setLastName(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 2: {
                            candidate.setBirthYear((int) Double.parseDouble(cellValue.toString()));
                            break;
                        }
                        case 3: {
                            String personalNumber = Utils.handleWhitespace(cellValue.toString().trim());
                            String personalNumberPattern = "^\\d{3}-\\d{3}-\\d{3}-\\d{3}$";
                            if (personalNumber.length() == 0) {
                                candidate.setPersonalNumber("");
                            } else if (!Pattern.matches(personalNumberPattern, personalNumber)) {
                                throw new BadRequestException("error.personalNumberInvalid", null);
                            } else {
                                personalNumber = personalNumber.replaceAll("-", "");
                                candidate.setPersonalNumber(personalNumber);
                            }
                            break;
                        }
                        case 4: {
                            candidate.setGender(Gender.valueOf(cellValue.toString()));
                            break;
                        }
                        case 5: {
                            candidate.setRegion(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 6: {
                            candidate.setLinkLnLink(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 7: {
                            candidate.setEmployer(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 8: {
                            candidate.setFirstEmployment(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 9: {
                            candidate.setManagerialPosition(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 10: {
                            candidate.setBusinessAreas(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 11: {
                            candidate.setStreetAddress(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 12: {
                            int zipCode = (int) Double.parseDouble(cellValue.toString());
                            if (zipCode <= 0){
                                throw new BadRequestException("error.zipCodeNegative", null);
                            }
                            candidate.setZipCode(zipCode);
                            break;
                        }
                        case 13: {
                            candidate.setCity(Utils.handleWhitespace(cellValue.toString()));
                            break;
                        }
                        case 14: {
                            String phoneNumber = String.valueOf((int) Double.parseDouble(Utils.handleWhitespace(cellValue.toString())));
                            String phoneNumberPattern = "^\\d{10,12}$";
                            if (!Pattern.matches(phoneNumberPattern, phoneNumber)){
                                throw new BadRequestException("error.phoneNumberInvalid", null);
                            }
                            candidate.setPhoneNumber(phoneNumber);
                            break;
                        }
                        case 15: {
                            String emailPrivate = Utils.handleWhitespace(cellValue.toString());
                            if (!Utils.isValidEmail(emailPrivate)){
                                throw new BadRequestException("error.emailPrivateInvalid", null);
                            }
                            candidate.setEmailPrivate(emailPrivate);
                            break;
                        }
                        case 16: {
                            String emailWork = Utils.handleWhitespace(cellValue.toString());
                            if (!Utils.isValidEmail(emailWork)){
                                throw new BadRequestException("error.emailWorkInvalid", null);
                            }
                            candidate.setEmailWork(emailWork);
                            break;
                        }
                    }
                }
                candidateListToAdd.add(candidate);
            }
            workbook.close();
            inputStream.close();

            return candidateListToAdd;
        } catch (IOException e) {
            throw new BadRequestException("error.uploadExcelWrongFormat", null);
        }
    }

    /**
     * Get cell value with cases
     * @param cell: cell to get value
     * @return object contain value of cell
     */
    private static Object getCellValue (Cell cell){
        CellType cellType = cell.getCellType();
        Object cellValue = null;
        switch (cellType) {
            case BOOLEAN:
                cellValue = cell.getBooleanCellValue();
                break;
            case FORMULA:
                Workbook workbook = cell.getSheet().getWorkbook();
                FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
                cellValue = evaluator.evaluate(cell).getNumberValue();
                break;
            case NUMERIC:
                cellValue = cell.getNumericCellValue();
                break;
            case STRING:
                cellValue = cell.getStringCellValue();
                break;
            case _NONE:
            case BLANK:
            case ERROR:
                throw new BadRequestException("error.cellContentError", String.valueOf(cell.getColumnIndex()));
            default:
                break;
        }
        return cellValue;
    }

    public Page<CandidateDTO> findCandidates(Pageable pageable) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        Page<Candidate> page = candidateRepository.findCandidateByDeletedIsFalse(pageable);
        return page.map(CandidateDTO::new);
    }

    public CandidateDTO getDetailCandidate(long id) {
        Candidate candidate = Utils.requireExists(candidateRepository.findById(id), "error.notFound");
        return new CandidateDTO(candidate);
    }

    public Page<CandidateDTO> findCandidatesByCriteria(Pageable pageable, CandidateDTO candidateDTO, String keyword) {
        if (!userACL.isUser()){
            throw new AccessForbiddenException("error.notUser");
        }
        Page<Candidate> page = candidateCriteriaRepository.findCandidateCriteria(pageable, candidateDTO, keyword);
        if (!StringUtils.isBlank(keyword)) {
            tagService.save(keyword);
        }
        Page<CandidateDTO> results = page.map(CandidateDTO::new);
        return results;
    }

    public CandidateDTO updateCvsForCandidate(Long idCandidate, List<CvsDTO> listCvsDTO){
        if (!userACL.isAdmin()){
            throw new AccessForbiddenException("error.notAdmin");
        }
        Candidate candidate = Utils.requireExists(candidateRepository.findById(idCandidate), "error.notFoundCandidate");
        List<CandidateCv> candidateCvList = candidateCvRepository.findCandidateCvByCandidate(candidate);
        candidateCvRepository.deleteAll(candidateCvList);
        candidateCvList.removeAll(candidateCvList);
        List<CvsDTO> cvsDTOList = new ArrayList<>();
        for(CvsDTO cvsDTO : listCvsDTO){
            Cvs cvs = Utils.requireExists(cvsRepository.findCvsByIdAndDeletedIsFalse(cvsDTO.getId()), "error.notFoundCvs");
            CandidateCv candidateCv = new CandidateCv();
            candidateCv.setCandidate(candidate);
            candidateCv.setCvs(cvs);
            candidateCvList.add(candidateCv);
            cvsDTOList.add(new CvsDTO(cvs));
        }
        candidateCvRepository.saveAll(candidateCvList);
        CandidateDTO candidateDTO = new CandidateDTO(candidate);
        candidateDTO.setCvsDTOList(cvsDTOList);
        return candidateDTO;
    }
}
