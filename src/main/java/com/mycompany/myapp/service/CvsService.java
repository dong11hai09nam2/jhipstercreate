package com.mycompany.myapp.service;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.mycompany.myapp.domain.Cvs;
import com.mycompany.myapp.repository.CvsRepository;
import com.mycompany.myapp.service.dto.CvsDTO;
import com.mycompany.myapp.service.error.AccessForbiddenException;
import com.mycompany.myapp.service.error.BadRequestException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class CvsService {
    private final UserACL userACL;

    private final CvsRepository cvsRepository;

    private final TagService tagService;

    public CvsService(UserACL userACL, CvsRepository cvsRepository, TagService tagService) {
        this.userACL = userACL;
        this.cvsRepository = cvsRepository;
        this.tagService = tagService;
    }

    public List<CvsDTO> save(MultipartFile multipartFile) throws IOException {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        List<Cvs> cvsList = new ArrayList<>();
        String filePath = multipartFile.getOriginalFilename();
        String fileName = filePath.substring(0, filePath.indexOf("."));
        String contentCV = readContentCV(multipartFile.getInputStream());
        Cvs cv1 = new Cvs();
        cv1.setName(fileName);
        cv1.setContent(contentCV);
        cvsList.add(cv1);
        cvsRepository.saveAll(cvsList);
        return cvsList.stream().map(CvsDTO::new).collect(Collectors.toList());
    }

    private String readContentCV(InputStream inputStream){
        PdfReader pdfReader;
        String contentCV = "";
        try {
            pdfReader = new PdfReader(inputStream);
            int numberOfCv = pdfReader.getNumberOfPages();
            for (int i = 1; i <= numberOfCv; i++) {
                contentCV += PdfTextExtractor.getTextFromPage(pdfReader, i);
            }
            readingTag(contentCV);
        } catch (IOException e) {
            throw new BadRequestException("error.cvError", null);
        }
        return contentCV;
    }

    private void readingTag(String content) {
        String str = "^[a-zA-Z0-9áàảãạăắằẳẵặâấầẩẫậđéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợùúủũụưứừửữựýỳỷỹỵ]";
        content = content.replaceAll("[-/.,\\n]", " ");
        String[] strings = content.split(" ");
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].length() <= 0) {
                continue;
            }
            if (Pattern.matches(str, String.valueOf(strings[i]))) {
                stringList.add(strings[i]);
            }
        }
        tagService.saveTagsWithReadingTag(stringList);
    }
}
