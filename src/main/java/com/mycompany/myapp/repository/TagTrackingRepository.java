package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.domain.TagTracking;
import com.mycompany.myapp.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TagTrackingRepository extends JpaRepository<TagTracking, Long> {
    Optional<TagTracking> findTagTrackingByUserAndTag(User user, Tag tag);

    long countTagTrackingByTag(Tag tag);
}
