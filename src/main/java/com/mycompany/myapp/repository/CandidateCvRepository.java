package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.domain.CandidateCv;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidateCvRepository extends JpaRepository<CandidateCv, Long> {
    List<CandidateCv> findCandidateCvByCandidate(Candidate candidate);
}
