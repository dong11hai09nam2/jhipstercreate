package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Tag;
import com.mycompany.myapp.service.dto.TagDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    Optional<Tag> findTagByValue(String value);

    Page<TagDTO> findTagsByDeletedIsFalseAndActiveIsTrue(Pageable pageable);

    Optional<Tag> findTagsByIdAndDeletedIsFalse(long id);

    Optional<Tag> findTagsByIdAndDeletedIsFalseAndBlockTagIsTrue(long id);
}
