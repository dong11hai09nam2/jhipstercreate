package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Candidate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CandidateRepository extends JpaRepository<Candidate, Long> {
    Page<Candidate> findCandidateByDeletedIsFalse(Pageable pageable);
}
