package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.service.dto.CandidateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class CandidateCriteriaRepository {
    final private EntityManager entityManager;
    final private CriteriaBuilder criteriaBuilder;

    public CandidateCriteriaRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    public Page<Candidate> findCandidateCriteria(Pageable pageable, CandidateDTO candidateDTO, String keyword) {
        CriteriaQuery<Candidate> criteriaQuery = criteriaBuilder.createQuery(Candidate.class);
        Root<Candidate> candidateRoot = criteriaQuery.from(Candidate.class);

        Predicate predicate = getPredicate(candidateDTO, candidateRoot);

        Predicate predicateKeyword = null;
        long candidateCount;

        if (keyword != null) {
            predicateKeyword = getPredicateByKeyword(keyword, candidateRoot);
            criteriaQuery.where(predicate, predicateKeyword);
        } else {
            criteriaQuery.where(predicate);
        }
        TypedQuery<Candidate> typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        typedQuery.setMaxResults(pageable.getPageSize());

        candidateCount = getCandidateCount(predicateKeyword, predicate);

        return new PageImpl<>(typedQuery.getResultList(), pageable, candidateCount);
    }

    private Predicate getPredicate(CandidateDTO candidateDTO, Root<Candidate> candidateRoot) {
        List<Predicate> predicates = new ArrayList<>();

        if (Objects.nonNull(candidateDTO.getFirstName())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("firstName"), "%" + candidateDTO.getFirstName() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getLastName())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("lastName"), "%" + candidateDTO.getLastName() + "%"));
        }
        if (candidateDTO.getBirthYear() != 0) {
            predicates.add(criteriaBuilder.equal(candidateRoot.get("birthYear"), candidateDTO.getBirthYear()));
        }
        if (Objects.nonNull(candidateDTO.getPersonalNumber())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("personalNumber"), "%" + candidateDTO.getPersonalNumber() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getGender())) {
            predicates.add(criteriaBuilder.equal(candidateRoot.get("gender"), candidateDTO.getGender()));
        }
        if (Objects.nonNull(candidateDTO.getLinkLnLink())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("linkLnLink"), "%" + candidateDTO.getLinkLnLink() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getFirstEmployment())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("firstEmployment"), "%" + candidateDTO.getFirstEmployment() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getManagerialPosition())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("managerialPosition"), "%" + candidateDTO.getManagerialPosition() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getBusinessAreas())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("businessAreas"), "%" + candidateDTO.getBusinessAreas() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getStreetAddress())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("streetAddress"), "%" + candidateDTO.getStreetAddress() + "%"));
        }
        if (candidateDTO.getZipCode() != 0) {
            predicates.add(criteriaBuilder.equal(candidateRoot.get("zipCode"), candidateDTO.getZipCode()));
        }
        if (Objects.nonNull(candidateDTO.getPhoneNumber())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("phoneNumber"), "%" + candidateDTO.getPhoneNumber() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getEmailPrivate())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("emailPrivate"), "%" + candidateDTO.getEmailPrivate() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getEmailWork())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("emailWork"), "%" + candidateDTO.getEmailWork() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getCity())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("city"), "%" + candidateDTO.getCity() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getEmployer())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("employer"), "%" + candidateDTO.getEmployer() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getRegion())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("region"), "%" + candidateDTO.getRegion() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getCountry())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("country"), "%" + candidateDTO.getCountry() + "%"));
        }
        if (Objects.nonNull(candidateDTO.getFirstName())) {
            predicates.add(criteriaBuilder.like(candidateRoot.get("firstName"), "%" + candidateDTO.getFirstName() + "%"));
        }
        predicates.add(criteriaBuilder.equal(candidateRoot.get("deleted"), false));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

    private Predicate getPredicateByKeyword(String keyword, Root<Candidate> candidateRoot) {
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(criteriaBuilder.like(candidateRoot.get("firstName"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("lastName"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("personalNumber"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("region"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("country"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("linkLnLink"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("employer"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("firstEmployment"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("managerialPosition"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("businessAreas"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("streetAddress"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("city"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("phoneNumber"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("emailPrivate"), "%" + keyword + "%"));
        predicates.add(criteriaBuilder.like(candidateRoot.get("emailWork"), "%" + keyword + "%"));

        try {
            int keywordInt = Integer.parseInt(keyword);
            predicates.add(criteriaBuilder.like(candidateRoot.get("birthYear"), "%" + keywordInt + "%"));
            predicates.add(criteriaBuilder.like(candidateRoot.get("zipCode"), "%" + keywordInt + "%"));
        } finally {
            return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
        }
    }

    private long getCandidateCount(Predicate predicate1, Predicate predicate2) {
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(long.class);
        Root<Candidate> countRoot = countQuery.from(Candidate.class);
        if (predicate1 == null) {
            countQuery.select(criteriaBuilder.count(countRoot)).where(predicate2);
        } else {
            countQuery.select(criteriaBuilder.count(countRoot)).where(predicate1, predicate2);
        }
        return entityManager.createQuery(countQuery).getSingleResult();
    }
}
