package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Cvs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CvsRepository extends JpaRepository<Cvs, Long> {
    Optional<Cvs> findCvsByIdAndDeletedIsFalse(long id);
}
