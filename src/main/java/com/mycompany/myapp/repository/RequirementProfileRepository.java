package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.RequirementProfile;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequirementProfileRepository extends JpaRepository<RequirementProfile, Long> {
    Page<RequirementProfileDTO> findRequirementProfileByUser(User user, Pageable pageable);
}
