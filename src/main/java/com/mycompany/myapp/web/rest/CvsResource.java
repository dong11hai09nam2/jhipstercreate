package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.CvsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class CvsResource {
    private final CvsService cvsService;

    public CvsResource(CvsService cvsService) {
        this.cvsService = cvsService;
    }

    @ApiOperation(
        value = "Save a Cvs"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created - create successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PostMapping("/cvs")
    public void saveCvs(@RequestParam(name = "multipartFile") MultipartFile multipartFile) throws IOException {
        cvsService.save(multipartFile);
    }
}
