package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.RequirementProfileService;
import com.mycompany.myapp.service.dto.RequirementProfileDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class RequirementProfileResource {

    private final RequirementProfileService requirementProfileService;

    public RequirementProfileResource(RequirementProfileService requirementProfileService) {
        this.requirementProfileService = requirementProfileService;
    }

    @ApiOperation(
        value = "Create a Requirement Profile"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created - create successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    @PostMapping("/requirementProfile")
    public ResponseEntity<RequirementProfileDTO> createRequirementProfile(@Valid @RequestBody RequirementProfileDTO requirementProfileDTO) {
        RequirementProfileDTO result = requirementProfileService.saveRequirementProfile(requirementProfileDTO);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @ApiOperation(
        value = "Update Requirement Profile"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Ok - update successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER +"\")")
    @PostMapping("/requirementProfile/update")
    public ResponseEntity<RequirementProfileDTO> updateRequirementProfile(@Valid @RequestBody RequirementProfileDTO requirementProfileDTO) {
        RequirementProfileDTO result = requirementProfileService.updateRequirementProfile(requirementProfileDTO);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Delete Requirement Profile"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Ok - delete successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    @DeleteMapping("/requirementProfile/delete")
    public void deleteRequirementProfile(@RequestParam long id) {
        requirementProfileService.deleteRequirementProfile(id);
    }

    @ApiOperation(
        value = "Get list Requirement profile"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Ok - delete successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    @GetMapping("/requirementProfiles")
    public ResponseEntity<List<RequirementProfileDTO>> getRequirementProfilesList(Pageable pageable) {
        final Page<RequirementProfileDTO> page = requirementProfileService.getListRequirementProfile(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
