package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.TagRepository;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.TagService;
import com.mycompany.myapp.service.dto.TagDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TagResource {
    private final TagService tagService;

    public TagResource(TagService tagService) {
        this.tagService = tagService;
    }

    @ApiOperation(
        value = "Get list tag"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created - create successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @GetMapping("/tags")
    public ResponseEntity<List<TagDTO>> getTags(Pageable pageable) {
        final Page<TagDTO> page = tagService.getListTag(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Create a Tag"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created - create successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @PostMapping("/tag")
    public ResponseEntity<TagDTO> createTag(@RequestParam String keyword) {
        TagDTO result = tagService.saveWithAdmin(keyword);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @ApiOperation(
        value = "Update a Tag"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Ok - update successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @PostMapping("/tag/update")
    public ResponseEntity<TagDTO> updateTag(@Valid @RequestBody TagDTO tagDTO) {
        TagDTO result = tagService.updateWithAdmin(tagDTO);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Delete a Tag"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Ok - delete successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @DeleteMapping("/tag/delete")
    public void deleteTag(@RequestParam long id) {
        tagService.deleteTagWithAdmin(id);
    }

    @ApiOperation(
        value = "Create a Block Tag"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created - create successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @PostMapping("/blockTag")
    public ResponseEntity<TagDTO> createBlockTag(@RequestParam String value) {
        TagDTO result = tagService.saveBlockTag(value);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @ApiOperation(
        value = "Update a Block Tag"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Ok - update successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @PostMapping("/blockTag/update")
    public ResponseEntity<TagDTO> updateBlockTag(@Valid @RequestBody TagDTO tagDTO) {
        TagDTO result = tagService.updateBlockTag(tagDTO);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Delete a Block Tag"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Ok - delete successfully"),
        @ApiResponse(code = 404, message = "Not found - data not found"),
        @ApiResponse(code = 403, message = "Forbidden - Not user permission to create. key: error.notUser"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @DeleteMapping("/blockTag/delete")
    public void deleteBlockTag(@RequestParam long id) {
        tagService.deleteBlockTag(id);
    }

    @ApiOperation(
        value = "Switch Tag to Block"
    )
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @DeleteMapping("/swicthTagToBlock")
    public ResponseEntity<TagDTO> switchTagToBlock(@RequestParam String value) {
        TagDTO result = tagService.switchTagToBlock(value);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Switch Block to Tag"
    )
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @DeleteMapping("/switchBlockToTag")
    public ResponseEntity<TagDTO> switchBlockToTag(@RequestParam long id) {
        TagDTO result = tagService.switchBlockToTag(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
