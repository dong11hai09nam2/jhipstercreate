package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.repository.CandidateCriteriaRepository;
import com.mycompany.myapp.security.AuthoritiesConstants;
import com.mycompany.myapp.service.CandidateService;
import com.mycompany.myapp.service.dto.CandidateDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CandidateResource {
    private final CandidateService candidateService;

    public CandidateResource(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @ApiOperation(
        value = "Create a candidate"
    )
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created - create successfully"),
        @ApiResponse(code = 400, message = "Bad request - request invalid"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
    })
    @PostMapping("/candidate")
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<CandidateDTO> createCandidate(@Valid @RequestBody CandidateDTO candidateDTO){
        CandidateDTO result = candidateService.saveCandidate(candidateDTO);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @ApiOperation(
        value = "Import list candidate"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - import successfully"),
        @ApiResponse(code = 400, message = "Bad Request - Upload excel failed or excel validation caught"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN + "\")")
    @PostMapping("/candidate/import")
    public void importCandidateViaExcel(@RequestParam("file") MultipartFile file) throws IOException {
        candidateService.importCandidateList(file.getInputStream());
    }

    @ApiOperation(
        value = "Get list candidate"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.ADMIN +"\")")
    @GetMapping("/candidates")
    public ResponseEntity<List<CandidateDTO>> getCandidatesList(Pageable pageable) {
        final Page<CandidateDTO> page = candidateService.findCandidates(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @ApiOperation(
        value = "Get list Candidate by criteria"
    )
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK - get successfully"),
        @ApiResponse(code = 403, message = "Forbidden - Not admin permission to create. key: error.notAdmin"),
        @ApiResponse(code = 500, message = "Internal Error - There is error during process, you should try again and contact with developer in this case")
    })
    @PreAuthorize("hasAuthority(\"" + AuthoritiesConstants.USER + "\")")
    @GetMapping("/criteria/candidate")
    public ResponseEntity<List<CandidateDTO>> getCandidateByCriteria(Pageable pageable, CandidateDTO candidateDTO, @RequestParam(required = false) String keyword) {
        final Page<CandidateDTO> page = candidateService.findCandidatesByCriteria(pageable, candidateDTO, keyword);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
